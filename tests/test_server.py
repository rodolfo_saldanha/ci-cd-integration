import unittest

from flask_server.flask_server import app


class TestIntegration(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_application(self):
        """
        Checks if the application returns the correct response
        """
        self.response = self.app.get('/')
        self.assertEqual(self.response.get_data(as_text=True), "hello world")
