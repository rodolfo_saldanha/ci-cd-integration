# Gitlab CI/CD Basics

Answer to the Exercises on [Issue #30](https://gitlab.com/DareData-open-source/tutorials/-/issues/30#exercises). The project stands up an EC2 instance and deploys a Flask server in it.

## How to use

The pipeline `.gitlab-ci.yml` should take care of the deployment, but you have to have your EC2 instance running in a first place.

- `flask_server/` creates a flask server.
- `ansible/` provisions the EC2 instance.
- `terraform/` stands up a single EC2 machine AWS.
- `tests/` contains the integration and unit tests. 

To stand up your EC2 instance, you need to set your aws credential in your terminal and change the path to your public key in `terraform/prod.tfvars`. After this, you enter `terraform/` and run:

```bash 
source deploy.sh
```

Then, you need to change the path to your instance's private key in `ansible/hosts` in `webserver-setup`. After this, you can make an initial set up of your instance by entering the ansible directory and running the `initial-setup.yml` playbook.

```bash
cd ~/ansible
ansible-playbook -i hosts initial-setup.yml
```

Finally, run the following playbook to deploy the server.

```bash
ansible-playbook -i hosts deploy-server.yml
```

### Reminder

The pipeline will only work if you set a custom variable in your project settings. You go to your gitlab project and **Settings > CI/CD**, under **Variables** you **Expand**, and you add the following variable:
- `SSH_PRIVATE_KEY`: The private key to access your EC2 instance. 


## Requirements

Execute `requirements.txt` with `pip install -r requirements.txt`, and you will have all packages required in this project.
